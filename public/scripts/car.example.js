class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
     <div class="box me-3 mb-3" >
            <div class="frame">
                <img src="${this.image}" alt="" class="img-r">
                <h5 class="text-center">${this.manufacture}  (${this.model}) / ${this.type}</h5>
                <h6 class="fw-bold"><i class="bi bi-cash"></i>  Rp. ${this.rentPerDay} / Hari</h6>
                <p><i class="bi bi-people-fill"></i> ${this.capacity}</p>
                <p><i class="bi bi-gear-fill"></i> ${this.transmission}</p>
                <p><i class="bi bi-calendar-check-fill"></i> ${this.year}</p>
                <small>${this.description}</small>
            </div>
            <button class="pilih fw-bold text-white">Pilih Mobil</button>
      </div>
    `;
  }
}
