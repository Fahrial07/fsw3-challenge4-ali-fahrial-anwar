class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    // this.loadButton = document.getElementById("load-btn");
    // this.carContainerElement = document.getElementById("cars-container");
    this.resultContainerElement = document.getElementById("result-container");
    this.driverType = document.getElementById("driverType");
    this.dateAvailable = document.getElementById("dateAvailable");
    this.timeAvailable = document.getElementById("Time");
    this.passengerCapacity = document.getElementById("passengerCapacity");
    this.btnSearch = document.getElementById("btn-search");
  }

  async init() {
    await this.load();
    // this.clear();
    // Register click listener
    // this.loadButton.onclick = this.search;
    this.btnSearch.onclick = this.searchData;
  }

  searchData = () => {
    this.clear();
  const cars = Car.list

   let d = new Date(this.dateAvailable.value);
  const t = this.timeAvailable.value;
  d.setHours(t)
  cars.forEach(cars => {
        if(cars.capacity > this.passengerCapacity.value && new Date(cars.availableAt) > d) {
             const node = document.createElement("div");
        node.innerHTML = cars.render();
      this.resultContainerElement.appendChild(node);
        }
      });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.resultContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.resultContainerElement.firstElementChild;
    }
  };
}
