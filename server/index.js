// // console.log("Implement servermu disini yak 😝!");const http = require('http');
const http = require('http');
const { PORT = 8000 } = process.env;

const fs = require('fs');
const path = require('path');
const url = require('url')
const PUBLIC_DIRECTORY = path.join(__dirname, '../public'); // public/home.html

function onRequest(req, res) {
    let requestUrl = req.url;

    switch (requestUrl) {
        case "/":
            requestUrl = "/index.html";
            break;
        case "/cars":
            requestUrl = "cars.html";
            break;
        default:
            requestUrl = req.url;
            break;
    }

    const parseUrl = url.parse(requestUrl);
    const pathName = `${parseUrl.pathname}`;
    const extension = path.parse(pathName).ext;
    const absolutePath = path.join(PUBLIC_DIRECTORY, pathName);
    // console.log(`extension`, extension);
    // console.log(`absolute`, absolutePath);

    const ext = {
        ".css": "text/css",
        ".jpg": "image/jpg",
        ".svg": "image/svg+xml",
        ".html": "text/html",
        ".js": "text/javascript",
        ".app": "application/xml",
    }

    fs.exists(absolutePath, (exists) => {
        if (!exists) {
            res.writeHead(403);
            res.write("FILE NOT FOUND")
            return;
        }
    })

    fs.readFile(absolutePath, (err, data) => {
        if (err) {
            res.statusCode = 500;
            res.write("FILE NOT FOUND");
            console.log(err);
        } else {
            res.setHeader('Content-Type', ext[extension] || "text/plan");
            res.end(data)
        }
    });
}

const server = http.createServer(onRequest);

server.listen(PORT, 'localhost', () => {
    console.log(`Server sudah berjalan pada port : ${PORT}...`);
})



